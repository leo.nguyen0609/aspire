<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/27/2019
 * Time: 1:39 PM
 */

namespace App\Api\Models;


use App\Eloquents\Loan;

class LoanModel
{
    public static function create(array $data)
    {
        $loan = new Loan();

        foreach ($data as $key => $value) {
            $loan->{$key} = $value;
        }

        $loan->save();

        return $loan;
    }
}