<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/27/2019
 * Time: 1:39 PM
 */

namespace App\Api\Models;


use App\Eloquents\Repayment;

class RepaymentModel
{
    const IS_NOT_PAID = 0;
    const IS_PAID = 1;

    public static function create(array $data)
    {
        $payment = new Repayment();

        foreach ($data as $key => $value) {
            $payment->{$key} = $value;
        }

        $payment->save();

        return $payment;
    }

    public static function update(array $data)
    {
        $payment = new Repayment();

        $record = $payment->where('user_id', $data['user_id'])
            ->where('loan_id', $data['loan_id'])
            ->where('term', $data['term'])
            ->where('is_paid', self::IS_NOT_PAID)
            ->first();

        if (!empty($record)) {
            $record->is_paid = $data['is_paid'] ?? null;
            $record->paid_date = $data['paid_date'] ?? null;
            $record->amount = $data['amount'] ?? null;
            $record->update();
        }

        return $record;
    }

    public static function checkPaid(int $user_id, int $loan_id, int $term)
    {
        $payment = new Repayment();

        $data = $payment->where('user_id', $user_id)
            ->where('loan_id', $loan_id)
            ->where('term', $term)
            ->where('is_paid', self::IS_PAID)
            ->first();

        return $data;
    }
}