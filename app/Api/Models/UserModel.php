<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/27/2019
 * Time: 1:14 PM
 */

namespace App\Api\Models;


use App\Eloquents\User;

class UserModel
{
    public static function create(array $data)
    {
        $user = new User();

        foreach ($data as $key => $value) {
            $user->{$key} = $value;
        }

        $user->save();

        return $user;

    }
}