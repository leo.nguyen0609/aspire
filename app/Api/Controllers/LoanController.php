<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/27/2019
 * Time: 1:34 PM
 */

namespace App\Api\Controllers;


use App\Api\Models\LoanModel;
use App\Api\Models\RepaymentModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoanController extends Controller
{
    public function createLoan(Request $request)
    {
        try {
            $input = $request->all();
            $msg = $this->_validateInput($input);
            if (!empty($msg)) {
                return response($msg, 400);
            }
            $data = $this->_getParams($input);
            $loan = LoanModel::create($data);
            $input['loan_id'] = $loan->loan_id;
            $paymentData = $this->_getPaymentParams($input);
            RepaymentModel::create($paymentData);

            return response()->json($loan);
        } catch (\Exception $ex) {
            return response($ex->getMessage(), 400);
        }
    }

    private function _getParams($input)
    {
        $loan = [];
        $loan['loan_name'] = $input['loan_name'];
        $loan['duration'] = $input['duration'] ?? null;
        $loan['frequency'] = $input['frequency'] ?? null;
        $loan['interest'] = $input['interest'] ?? null;
        $loan['arrangement'] = $input['arrangement'] ?? null;

        return $loan;
    }

    private function _getPaymentParams($input)
    {
        $payment = [];
        $payment['user_id'] = (int)$input['user_id'];
        $payment['loan_id'] = (int)$input['loan_id'];
        $payment['term'] = (int)$input['term'];
        $payment['paid_date'] = !empty($input['paid_date']) ? strtotime($input['paid_date']) : null;
        $payment['amount'] = $input['amount'] ?? null;
        $payment['is_paid'] = RepaymentModel::IS_NOT_PAID;

        return $payment;
    }

    private function _validateInput($input)
    {
        if (empty($input)) {
            return "Input is empty";
        }
        if (empty($input['user_id'])) {
            return "user_id is empty";
        }
        if (empty($input['loan_name'])) {
            return "Loan name is empty";
        }
        if (empty($input['term'])) {
            return "Term is empty";
        }

        return null;
    }

}