<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/28/2019
 * Time: 1:43 PM
 */

namespace App\Api\Controllers;


use App\Api\Models\UserModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function createUser(Request $request)
    {
        try {
            $input = $request->all();
            $msg = $this->_validateInput($input);
            if (!empty($msg)) {
                return response($msg, 400);
            }
            $data = $this->_getParams($input);
            $user = UserModel::create($data);

            return response()->json($user);
        } catch (\Exception $ex) {
            return response($ex->getMessage(), 400);
        }
    }

    private function _getParams($input)
    {
        $user = [];
        $user['username'] = $input['username'];
        $user['address'] = $input['address'] ?? null;
        $user['phone'] = $input['phone'];
        $user['birthday'] = strtotime($input['birthday']) ?? null;
        $user['email'] = $input['email'] ?? null;
        $user['gender'] = $input['gender'];

        return $user;
    }

    private function _validateInput($input)
    {
        if (empty($input)) {
            return "Input is empty";
        }
        if (empty($input['username'])) {
            return "Name is empty";
        }

        if (!is_numeric($input['phone'])) {
            return "Phone should be a number";
        }

        if (!is_numeric($input['gender'])) {
            return "Gender should be 0 or 1";
        }

        if(!empty($input['email'])){
            $email_code = $this->_validateEmail($input['email']);
            if($email_code > 0) {
                return "Email is invalid";
            }
        }

        return null;
    }

    private function _validateEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return 1;
        }

        return 0;
    }
}