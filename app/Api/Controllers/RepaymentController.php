<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/28/2019
 * Time: 3:50 PM
 */

namespace App\Api\Controllers;


use App\Api\Models\RepaymentModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RepaymentController
{
    public function updatePayment(Request $request)
    {
        try {
            $input = $request->all();
            $msg = $this->_validateInput($input);
            if (!empty($msg)) {
                return response($msg, 400);
            }
            $data = $this->_getParams($input);
            $data['is_paid'] = RepaymentModel::IS_PAID;
            $repayment = RepaymentModel::update($data);

            return response()->json($repayment);
        } catch (\Exception $ex) {
            return response($ex->getMessage(), 400);
        }
    }

    private function _getParams($input)
    {
        $payment = [];
        $payment['user_id'] = (int)$input['user_id'];
        $payment['loan_id'] = (int)$input['loan_id'];
        $payment['term'] = (int)$input['term'];
        $payment['amount'] = $input['amount'] ?? null;

        return $payment;
    }

    private function _validateInput($input)
    {
        if (empty($input)) {
            return "Input is empty";
        }
        if (empty($input['user_id'])) {
            return "user_id is empty";
        }
        if (empty($input['loan_id'])) {
            return "loan_id is empty";
        }
        if (empty($input['term'])) {
            return "term is empty";
        }
        if (empty($input['amount'])) {
            return "amount is empty";
        }

        if ($this->validatePaid($input) > 0) {
            return "You have paid the loan in this term already!";
        }

        return null;
    }

    private function validatePaid($input)
    {
        $payment = RepaymentModel::checkPaid($input['user_id'], $input['loan_id'], $input['term']);
        if (!empty($payment)) {
            return 1;
        }

        return 0;
    }
}