<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/27/2019
 * Time: 1:30 PM
 */

namespace App\Eloquents;


class Loan extends BaseEloquent
{

    protected $table = 'loan';

    protected $primaryKey = 'loan_id';

    protected $fillable = [
        'loan_name',
        'duration',
        'frequency',
        'interest',
        'arrangement ',
        'updated_at',
        'created_at'
    ];

    public function repayment()
    {
        return $this->hasMany('Repayment');
    }


}