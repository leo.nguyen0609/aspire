<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/27/2019
 * Time: 1:32 PM
 */

namespace App\Eloquents;


class Repayment extends BaseEloquent
{

    protected $table = 'repayment';

    protected $primaryKey = 'repayment_id';

    protected $fillable = [
        'user_id',
        'loan_id',
        'term',
        'paid_date',
        'amount',
        'remain',
        'is_paid',
        'updated_at',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function loan()
    {
        return $this->belongsTo('Loan');
    }

}