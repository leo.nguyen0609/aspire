<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/27/2019
 * Time: 12:58 PM
 */

namespace App\Eloquents;


class User extends BaseEloquent
{

    protected $table = 'user';

    protected $primaryKey = 'user_id';

    protected $fillable = [
        'username',
        'address',
        'phone',
        'birthday',
        'email',
        'gender',
        'updated_at',
        'created_at'
    ];

    public function repayment()
    {
        return $this->hasMany('Repayment');
    }

}