<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/28/2019
 * Time: 3:30 PM
 */

namespace App\Eloquents;


use Illuminate\Database\Eloquent\Model;

class BaseEloquent extends Model
{
    protected $dateFormat = 'U';

}