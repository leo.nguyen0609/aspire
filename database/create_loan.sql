CREATE TABLE loan (
	loan_id INT auto_increment,
	loan_name VARCHAR ( 100 ) NOT NULL,
	duration INT DEFAULT 1,
	frequency INT DEFAULT 1,
	interest NUMERIC NULL,
	arrangement VARCHAR ( 100 ) NULL,
	updated_at INT NULL,
	created_at INT NULL,
PRIMARY KEY ( loan_id ) 
)