CREATE TABLE USER (
	user_id INT auto_increment,
	username VARCHAR ( 100 ) NOT NULL,
	address VARCHAR ( 250 ) NULL,
	phone INT NOT NULL,
	birthday INT NULL,
	email VARCHAR ( 100 ) NULL,
	gender INT NULL,
	updated_at INT NULL,
	created_at INT NULL,
PRIMARY KEY ( user_id ) 
)