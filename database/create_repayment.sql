CREATE TABLE repayment (
	repayment_id INT auto_increment,
	user_id INT NOT NULL,
	loan_id INT NOT NULL,
	term INT NOT NULL DEFAULT 1,
	paid_date INT NULL,
	amount NUMERIC DEFAULT 0,
	remain NUMERIC DEFAULT 0,
	is_paid INT DEFAULT 0,
	updated_at INT NULL,
	created_at INT NULL,
PRIMARY KEY ( repayment_id ) 
)