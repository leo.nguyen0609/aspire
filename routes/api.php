<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/27/2019
 * Time: 1:10 PM
 */

$router->post('/user/create', [
    'as'   => 'createUser',
    'uses' => 'UserController@createUser'
]);

$router->post('/loan/create', [
    'as'   => 'createLoan',
    'uses' => 'LoanController@createLoan'
]);

$router->put('/payment/update', [
    'as'   => 'updatePayment',
    'uses' => 'RepaymentController@updatePayment'
]);
